import React, { Component } from "react";
import { appSocket } from "../../appSocket";

import { FaPaperPlane } from "react-icons/fa";

export default class Textinput extends Component {
  constructor() {
    super();
    this.state = {
      mensagem: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleChange(e) {
    this.setState({
      mensagem: e.currentTarget.value
    });
  }

  handleClick() {
    appSocket.emit("nova-mensagem", {
      nome: "Sistema",
      mensagem: this.state.mensagem
    });
    this.setState({
      mensagem: ""
    });
  }

  render() {
    return (
      <div className="box-sendmsg">
        <input
          className="msg-chat"
          onChange={this.handleChange}
          value={this.state.mensagem}
        />
        <React.Fragment>
          <button className="send-btn" onClick={this.handleClick}>
            <FaPaperPlane />
          </button>
        </React.Fragment>
      </div>
    );
  }
}
