import React, { Component } from "react";
import root from "react-shadow";
import { appSocket } from "../../appSocket";

import styles from "!raw-loader!../styled.css";

export default class Listmsg extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mensagens: [{ nome: `${this.props.username}`, mensagem: "Teste" }]
    };
    this.renderMensagens = this.renderMensagens.bind(this);
  }
  componentDidMount() {
    appSocket.on("mensagens", mensagens => {
      this.setState({
        mensagens
      });
    });
  }
  renderMensagens() {
    return this.state.mensagens.map((mensagem, i) => (
      <Mensagem key={i} {...mensagem} />
    ));
  }
  render() {
    return <ul>{this.renderMensagens()}</ul>;
  }
}
const Mensagem = function({ mensagem, nome }) {
  return (
    <root.div>
      <div className="msglist-master">
        <li className="msglist-list">
          <p className="msglist-p">
            <small>
              <i>{nome}</i>
            </small>
            <p>{mensagem}</p>
          </p>
        </li>
      </div>
      <style type="text/css">{styles}</style>
    </root.div>
  );
};
