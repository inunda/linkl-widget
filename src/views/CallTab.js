import React from "react";
import root from "react-shadow";
import styles from "!raw-loader!../styled.css";
import validator from "validator";

import http from "https";
import { linklcall } from "../utils/Apis";
import { myVariables } from "../utils/GlobalVariables";

import InputMask from "react-input-mask";

import SuccessTab from "./Success";

export default class CallTab extends React.Component {
  constructor() {
    super();
    this.state = {
      userName: "",
      userPhone: "",
      success: false,
      valid: false
    };
    this.userCall = this.userCall.bind(this);
    this.validFunc = this.validFunc.bind(this);
  }

  getName(e) {
    const userNameEvent = e.target.value;
    if (
      validator.isAlpha(userNameEvent) ||
      !validator.isEmpty(userNameEvent) ||
      (validator.isAlpha(userNameEvent) && !validator.isEmpty(userNameEvent))
    ) {
      this.setState({
        userName: userNameEvent
      });
    } else {
      this.setState({
        valid: false
      });
    }

    this.validFunc();
  }

  getPhone(e) {
    const userPhoneEvent = e.target.value
      .replace(/[{()}]/g, "")
      .replace("-", "")
      .replace(" ", "")
      .replace("  ", "");
    if (
      validator.isMobilePhone(userPhoneEvent) ||
      !validator.isEmpty(userPhoneEvent) ||
      (validator.isMobilePhone(userPhoneEvent) &&
        !validator.isEmpty(userPhoneEvent))
    ) {
      this.setState({
        userPhone: userPhoneEvent
      });
    } else {
      this.setState({
        valid: false
      });
    }

    this.validFunc();
  }

  validFunc() {
    const { userName, userPhone } = this.state;
    if (
      userName === "" ||
      userPhone === "" ||
      (userName === "" && userPhone === "")
    ) {
      this.setState({
        valid: false
      });
    } else {
      this.setState({
        valid: true
      });
    }
  }

  userCall() {
    const { userName, userPhone } = this.state;
    var req = http.request(
      linklcall,
      function(res) {
        var chunks = [];
        res.on("data", function(chunk) {
          chunks.push(chunk);
        });
        res.on(
          "end",
          function() {
            this.setState({
              success: true
            });
            setTimeout(
              function() {
                this.setState({
                  success: false
                });
              }.bind(this),
              2000
            );
          }.bind(this)
        );
      }.bind(this)
    );
    req.write(
      '{"codigo":"' +
        myVariables[1] +
        '","nome":"' +
        userName +
        '","fone":"' +
        userPhone +
        '","email":"","agendamento":""}'
    );
    req.end();
  }

  render() {
    const { success, valid } = this.state;
    return (
      <root.div className="container-inner container-call">
        {myVariables[1] === null || myVariables[1] === "" ? (
          ""
        ) : success === false ? (
          <div className="tab">
            <p className="tab-title">Receba uma ligação em até 28 segundos. </p>

            <input
              className="tab-input__name"
              type="text"
              placeholder="Nome"
              onInput={e => this.getName(e)}
            />
            <InputMask
              className="tab-input__phone"
              mask="(99) 99999 - 9999"
              maskChar=" "
              placeholder="Telefone"
              onInput={e => this.getPhone(e)}
            />
            <div className="tab-button">
              {valid === false ? (
                <button className="tab-button__button--disabled" disabled>
                  Ligar
                </button>
              ) : (
                <button className="tab-button__button" onClick={this.userCall}>
                  Ligar
                </button>
              )}
            </div>
            <p className="tab-footer__text">
              Caso todas as linhas estiverem ocupadas, retornaremos seu contato!
            </p>
            <style type="text/css">{styles}</style>
          </div>
        ) : (
          <div className="tab">
            <SuccessTab />
          </div>
        )}
      </root.div>
    );
  }
}
