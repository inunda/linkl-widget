const path = require("path");
const webpack = require("webpack");
const bundleOutputDir = "./dist";
var copyWebpackPlugin = require("copy-webpack-plugin");

module.exports = env => {
  const isDevBuild = !(env && env.prod);

  return [
    {
      entry: ["babel-polyfill", "./src/index.js"],
      output: {
        filename: "widget.js",
        path: path.resolve(bundleOutputDir)
      },
      devServer: {
        contentBase: bundleOutputDir
      },
      plugins: isDevBuild
        ? [
            new webpack.SourceMapDevToolPlugin(),
            new copyWebpackPlugin([{ from: "demo/" }])
          ]
        : [
            new webpack.optimize.UglifyJsPlugin({
              minimize: true,
              comments: false,
              compress: {
                warnings: false,
                drop_console: true
              }
            })
          ],
      module: {
        rules: [
          { test: /\.html$/i, use: "html-loader" },
          {
            test: /\.css$/i,
            use: [
              "style-loader",
              "css-loader" + (isDevBuild ? "" : "?minimize")
            ]
          },
          {
            test: /\.(js|jsx)$/i,
            exclude: /node_modules/,
            use: {
              loader: "babel-loader",
              options: {
                presets: [
                  [
                    "@babel/env",
                    {
                      targets: {
                        browsers: ["ie 6", "safari 7"]
                      }
                    }
                  ]
                ]
              }
            }
          }
        ]
      },
      resolve: {
        extensions: ["*", ".js", ".jsx"]
      }
    }
  ];
};
