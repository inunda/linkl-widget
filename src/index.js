import React from "react";
import ReactDOM from "react-dom";
import root from "react-shadow";
import http from "http";
import styles from "!raw-loader!./styled.css";
import { myVariables } from "./utils/GlobalVariables";
import { validFunc } from "./utils/Apis";

import CallTab from "./views/CallTab";
import MessageTab from "./views/Message";
import ScheduleTab from "./views/Schedule";
import WhatsTab from "./views/Whatsapp";
import Chat from "./views/Chat";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      tabone: false,
      tabtwo: false,
      tabthree: false,
      tabfour: false,
      userStatus: "",
      userCall: "",
      userHour: "",
      userWhats: "",
      modal: false
    };
    this.openTabOne = this.openTabOne.bind(this);
    this.openTabTwo = this.openTabTwo.bind(this);
    this.openTabThree = this.openTabThree.bind(this);
    this.openTabFour = this.openTabFour.bind(this);
    this.modalClose = this.modalClose.bind(this);
    this.modalOpen = this.modalOpen.bind(this);
  }

  componentDidMount() {
    this.getUserInfo();
  }

  getUserInfo() {
    const req = http.request(
      validFunc,
      function(res) {
        let chunks = [];

        res.on("data", function(chunk) {
          chunks.push(chunk);
        });

        res.on(
          "end",
          function() {
            let body = Buffer.concat(chunks);
            let { ...userinfo } = JSON.parse(body.toString());

            if (userinfo.ligacoes === "true" && userinfo.horario === "true") {
              this.setState({
                tabone: true,
                tabtwo: false,
                tabthree: false,
                tabfour: false
              });
            } else {
              this.setState({
                tabone: false,
                tabtwo: true,
                tabthree: false,
                tabfour: false
              });
            }
            this.setState({
              userStatus: userinfo.status,
              userCall: userinfo.ligacoes,
              userHour: userinfo.horario,
              userWhats: userinfo.whatsapp
            });
          }.bind(this)
        );
      }.bind(this)
    );

    req.write(JSON.stringify({ token: myVariables[1] }));
    req.end();
  }

  openTabOne() {
    this.setState({
      tabone: true,
      tabtwo: false,
      tabthree: false,
      tabfour: false
    });
  }

  openTabTwo() {
    this.setState({
      tabtwo: true,
      tabone: false,
      tabthree: false,
      tabfour: false
    });
  }

  openTabThree() {
    this.setState({
      tabthree: true,
      tabone: false,
      tabtwo: false,
      tabfour: false
    });
  }

  openTabFour() {
    this.setState({
      tabfour: true,
      tabone: false,
      tabtwo: false,
      tabthree: false
    });
  }

  modalClose() {
    this.setState({
      modal: false
    });
  }

  modalOpen() {
    this.setState({
      modal: true
    });
  }

  render() {
    const {
      tabone,
      tabtwo,
      tabthree,
      tabfour,
      userStatus,
      userCall,
      userHour,
      userWhats,
      modal
    } = this.state;

    return (
      <root.div>
        {modal === false && userStatus === "ativo" ? (
          <div className="master-icon">
            <div className="icon-pulse" />
            <div className="myicon" onClick={this.modalOpen}>
              <div className="sprite">
                <svg
                  className="phone-icon-an"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 448 560"
                  width="40px"
                  height="40px"
                >
                  <path d="M154.9 187.8c14.7 1.5 28.7-7 34.2-20.7l32.5-81c6-14.9.4-31.9-13.3-40.5l-65-40.5c-13.2-8.2-30.1-6.3-41.1 4.7C-34.3 146-34 366.4 102.2 502.3c11 11 27.9 12.9 41.1 4.7l65-40.5C222 458 227.5 441 221.6 426l-32.5-81c-5.5-13.6-19.5-22.1-34.2-20.7l-43.2 4.3c-14.5-47.2-14.5-97.9 0-145.1zM89.8 362.9l68.4-6.8c.5-.1 1 .2 1.2.7l32.5 81c.2.5 0 1.1-.5 1.4l-65 40.5c-.5.3-1.1.2-1.5-.2C1 356 1.1 155.9 124.9 32.4c.4-.4 1-.5 1.5-.2l65 40.5c.5.3.7.9.5 1.4l-32.5 81c-.2.5-.7.8-1.2.7L89.8 149c-28.7 79.1-27.6 137.9 0 213.9zm202.4-270l-6 5.7c-3.9 3.7-4.8 9.6-2.3 14.4 4.9 9.4 4.9 20.6 0 29.9-2.5 4.8-1.6 10.7 2.3 14.4l6 5.7c5.6 5.4 14.8 4.1 18.7-2.6 11.8-20 11.8-45 0-65.1-3.9-6.5-13-7.8-18.7-2.4zM357 49.2c-4.4-5.6-12.7-6.3-17.9-1.3l-5.8 5.6c-4.4 4.2-5 11.1-1.3 15.9 26.5 34.6 26.5 82.6 0 117.1-3.7 4.8-3.1 11.7 1.3 15.9l5.8 5.6c5.2 4.9 13.5 4.3 17.9-1.3 36.1-46.2 36.1-111.1 0-157.5zm45.9-44.9c-4.5-5.3-12.5-5.7-17.6-.9L379.5 9c-4.6 4.4-5 11.5-.9 16.4 49.7 59.5 49.6 145.9 0 205.4-4 4.8-3.6 12 .9 16.4l5.8 5.6c5 4.8 13.1 4.4 17.6-.9 60.2-71.8 60.1-176.1 0-247.6z" />
                </svg>
                <svg
                  className="clock-icon-an"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 448 580"
                  width="40px"
                  height="40px"
                >
                  <path d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm216 248c0 118.7-96.1 216-216 216-118.7 0-216-96.1-216-216 0-118.7 96.1-216 216-216 118.7 0 216 96.1 216 216zm-148.9 88.3l-81.2-59c-3.1-2.3-4.9-5.9-4.9-9.7V116c0-6.6 5.4-12 12-12h14c6.6 0 12 5.4 12 12v146.3l70.5 51.3c5.4 3.9 6.5 11.4 2.6 16.8l-8.2 11.3c-3.9 5.3-11.4 6.5-16.8 2.6z" />
                </svg>
                <svg
                  className="msg-icon-an"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 448 580"
                  width="40px"
                  height="40px"
                >
                  <path d="M448 0H64C28.7 0 0 28.7 0 64v288c0 35.3 28.7 64 64 64h96v84c0 7.1 5.8 12 12 12 2.4 0 4.9-.7 7.1-2.4L304 416h144c35.3 0 64-28.7 64-64V64c0-35.3-28.7-64-64-64zm32 352c0 17.6-14.4 32-32 32H293.3l-8.5 6.4L192 460v-76H64c-17.6 0-32-14.4-32-32V64c0-17.6 14.4-32 32-32h384c17.6 0 32 14.4 32 32v288zM280 240H136c-4.4 0-8 3.6-8 8v16c0 4.4 3.6 8 8 8h144c4.4 0 8-3.6 8-8v-16c0-4.4-3.6-8-8-8zm96-96H136c-4.4 0-8 3.6-8 8v16c0 4.4 3.6 8 8 8h240c4.4 0 8-3.6 8-8v-16c0-4.4-3.6-8-8-8z" />
                </svg>
                <svg
                  className="whats-icon-an"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="5 0 78 100"
                  width="40px"
                  height="40px"
                >
                  <path d="M90,43.841c0,24.213-19.779,43.841-44.182,43.841c-7.747,0-15.025-1.98-21.357-5.455L0,90l7.975-23.522   c-4.023-6.606-6.34-14.354-6.34-22.637C1.635,19.628,21.416,0,45.818,0C70.223,0,90,19.628,90,43.841z M45.818,6.982   c-20.484,0-37.146,16.535-37.146,36.859c0,8.065,2.629,15.534,7.076,21.61L11.107,79.14l14.275-4.537   c5.865,3.851,12.891,6.097,20.437,6.097c20.481,0,37.146-16.533,37.146-36.857S66.301,6.982,45.818,6.982z M68.129,53.938   c-0.273-0.447-0.994-0.717-2.076-1.254c-1.084-0.537-6.41-3.138-7.4-3.495c-0.993-0.358-1.717-0.538-2.438,0.537   c-0.721,1.076-2.797,3.495-3.43,4.212c-0.632,0.719-1.263,0.809-2.347,0.271c-1.082-0.537-4.571-1.673-8.708-5.333   c-3.219-2.848-5.393-6.364-6.025-7.441c-0.631-1.075-0.066-1.656,0.475-2.191c0.488-0.482,1.084-1.255,1.625-1.882   c0.543-0.628,0.723-1.075,1.082-1.793c0.363-0.717,0.182-1.344-0.09-1.883c-0.27-0.537-2.438-5.825-3.34-7.977   c-0.902-2.15-1.803-1.792-2.436-1.792c-0.631,0-1.354-0.09-2.076-0.09c-0.722,0-1.896,0.269-2.889,1.344   c-0.992,1.076-3.789,3.676-3.789,8.963c0,5.288,3.879,10.397,4.422,11.113c0.541,0.716,7.49,11.92,18.5,16.223   C58.2,65.771,58.2,64.336,60.186,64.156c1.984-0.179,6.406-2.599,7.312-5.107C68.398,56.537,68.398,54.386,68.129,53.938z" />
                </svg>
              </div>
            </div>
          </div>
        ) : (
          ""
        )}

        {modal === true && userStatus === "ativo" ? (
          <div
            className={
              modal === false
                ? "container container-op"
                : "container container-cs"
            }
          >
            <div className="container-in">
              <div className="close-modal" onClick={this.modalClose}>
                <p>x</p>
              </div>

              <ul className="tabs-nav">
                {userHour === "true" && userCall === "true" ? (
                  <li className="tab-item" onClick={this.openTabOne}>
                    <div className="phoneIcon-tab">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 448 560"
                        width="25px"
                        height="25px"
                      >
                        <path d="M154.9 187.8c14.7 1.5 28.7-7 34.2-20.7l32.5-81c6-14.9.4-31.9-13.3-40.5l-65-40.5c-13.2-8.2-30.1-6.3-41.1 4.7C-34.3 146-34 366.4 102.2 502.3c11 11 27.9 12.9 41.1 4.7l65-40.5C222 458 227.5 441 221.6 426l-32.5-81c-5.5-13.6-19.5-22.1-34.2-20.7l-43.2 4.3c-14.5-47.2-14.5-97.9 0-145.1zM89.8 362.9l68.4-6.8c.5-.1 1 .2 1.2.7l32.5 81c.2.5 0 1.1-.5 1.4l-65 40.5c-.5.3-1.1.2-1.5-.2C1 356 1.1 155.9 124.9 32.4c.4-.4 1-.5 1.5-.2l65 40.5c.5.3.7.9.5 1.4l-32.5 81c-.2.5-.7.8-1.2.7L89.8 149c-28.7 79.1-27.6 137.9 0 213.9zm202.4-270l-6 5.7c-3.9 3.7-4.8 9.6-2.3 14.4 4.9 9.4 4.9 20.6 0 29.9-2.5 4.8-1.6 10.7 2.3 14.4l6 5.7c5.6 5.4 14.8 4.1 18.7-2.6 11.8-20 11.8-45 0-65.1-3.9-6.5-13-7.8-18.7-2.4zM357 49.2c-4.4-5.6-12.7-6.3-17.9-1.3l-5.8 5.6c-4.4 4.2-5 11.1-1.3 15.9 26.5 34.6 26.5 82.6 0 117.1-3.7 4.8-3.1 11.7 1.3 15.9l5.8 5.6c5.2 4.9 13.5 4.3 17.9-1.3 36.1-46.2 36.1-111.1 0-157.5zm45.9-44.9c-4.5-5.3-12.5-5.7-17.6-.9L379.5 9c-4.6 4.4-5 11.5-.9 16.4 49.7 59.5 49.6 145.9 0 205.4-4 4.8-3.6 12 .9 16.4l5.8 5.6c5 4.8 13.1 4.4 17.6-.9 60.2-71.8 60.1-176.1 0-247.6z" />
                      </svg>
                    </div>
                    <p>Ligação</p>
                  </li>
                ) : (
                  <li className="tab-item" onClick={this.openTabTwo}>
                    <div className="clockIcon-tab">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="7 0 448 580"
                        width="25px"
                        height="25px"
                      >
                        <path d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm216 248c0 118.7-96.1 216-216 216-118.7 0-216-96.1-216-216 0-118.7 96.1-216 216-216 118.7 0 216 96.1 216 216zm-148.9 88.3l-81.2-59c-3.1-2.3-4.9-5.9-4.9-9.7V116c0-6.6 5.4-12 12-12h14c6.6 0 12 5.4 12 12v146.3l70.5 51.3c5.4 3.9 6.5 11.4 2.6 16.8l-8.2 11.3c-3.9 5.3-11.4 6.5-16.8 2.6z" />
                      </svg>
                    </div>
                    <p>Agendar</p>
                  </li>
                )}

                <li className="tab-item" onClick={this.openTabThree}>
                  <div className="msgIcon-tab">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="15 -25 448 580"
                      width="25px"
                      height="25px"
                    >
                      <path d="M448 0H64C28.7 0 0 28.7 0 64v288c0 35.3 28.7 64 64 64h96v84c0 7.1 5.8 12 12 12 2.4 0 4.9-.7 7.1-2.4L304 416h144c35.3 0 64-28.7 64-64V64c0-35.3-28.7-64-64-64zm32 352c0 17.6-14.4 32-32 32H293.3l-8.5 6.4L192 460v-76H64c-17.6 0-32-14.4-32-32V64c0-17.6 14.4-32 32-32h384c17.6 0 32 14.4 32 32v288zM280 240H136c-4.4 0-8 3.6-8 8v16c0 4.4 3.6 8 8 8h144c4.4 0 8-3.6 8-8v-16c0-4.4-3.6-8-8-8zm96-96H136c-4.4 0-8 3.6-8 8v16c0 4.4 3.6 8 8 8h240c4.4 0 8-3.6 8-8v-16c0-4.4-3.6-8-8-8z" />
                    </svg>
                  </div>
                  <p>Mensagem</p>
                </li>

                {userWhats !== "false" ? (
                  <li className="tab-item" onClick={this.openTabFour}>
                    <div className="whatsIcon-tab">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="5 0 78 100"
                        width="25px"
                        height="25px"
                      >
                        <path
                          id="WhatsApp"
                          d="M90,43.841c0,24.213-19.779,43.841-44.182,43.841c-7.747,0-15.025-1.98-21.357-5.455L0,90l7.975-23.522   c-4.023-6.606-6.34-14.354-6.34-22.637C1.635,19.628,21.416,0,45.818,0C70.223,0,90,19.628,90,43.841z M45.818,6.982   c-20.484,0-37.146,16.535-37.146,36.859c0,8.065,2.629,15.534,7.076,21.61L11.107,79.14l14.275-4.537   c5.865,3.851,12.891,6.097,20.437,6.097c20.481,0,37.146-16.533,37.146-36.857S66.301,6.982,45.818,6.982z M68.129,53.938   c-0.273-0.447-0.994-0.717-2.076-1.254c-1.084-0.537-6.41-3.138-7.4-3.495c-0.993-0.358-1.717-0.538-2.438,0.537   c-0.721,1.076-2.797,3.495-3.43,4.212c-0.632,0.719-1.263,0.809-2.347,0.271c-1.082-0.537-4.571-1.673-8.708-5.333   c-3.219-2.848-5.393-6.364-6.025-7.441c-0.631-1.075-0.066-1.656,0.475-2.191c0.488-0.482,1.084-1.255,1.625-1.882   c0.543-0.628,0.723-1.075,1.082-1.793c0.363-0.717,0.182-1.344-0.09-1.883c-0.27-0.537-2.438-5.825-3.34-7.977   c-0.902-2.15-1.803-1.792-2.436-1.792c-0.631,0-1.354-0.09-2.076-0.09c-0.722,0-1.896,0.269-2.889,1.344   c-0.992,1.076-3.789,3.676-3.789,8.963c0,5.288,3.879,10.397,4.422,11.113c0.541,0.716,7.49,11.92,18.5,16.223   C58.2,65.771,58.2,64.336,60.186,64.156c1.984-0.179,6.406-2.599,7.312-5.107C68.398,56.537,68.398,54.386,68.129,53.938z"
                        />
                      </svg>
                    </div>
                    <p>Whatsapp</p>
                  </li>
                ) : (
                  ""
                )}
              </ul>

              {userHour === "true" && userCall === "true" ? (
                tabone ? (
                  <CallTab />
                ) : (
                  ""
                )
              ) : tabtwo ? (
                <ScheduleTab />
              ) : (
                ""
              )}

              {tabthree ? <MessageTab /> : ""}
              {tabfour ? (
                userWhats !== "false" ? (
                  <WhatsTab userwhats={userWhats} />
                ) : (
                  ""
                )
              ) : (
                ""
              )}
            </div>
          </div>
        ) : (
          ""
        )}
        <style type="text/css">{styles}</style>
      </root.div>
    );
  }
}

let myDiv = document.createElement("div");
myDiv.id = "root";
document.body.appendChild(myDiv);

ReactDOM.render(<App />, document.getElementById("root"));
