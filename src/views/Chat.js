import React, { Component } from "react";
import root from "react-shadow";
import Textinput from "../components/Textinput";
import Listmsg from "../components/Listmsg";
import validator from "validator";
import InputMask from "react-input-mask";

import styles from "!raw-loader!../styled.css";

export default class Chat extends Component {
  constructor() {
    super();
    this.state = {
      enterChat: false,
      btnEnter: false,
      userPhone: "",
      userName: ""
    };
    this.getUserPhone = this.getUserPhone.bind(this);
    this.getUserName = this.getUserName.bind(this);
    this.validateFunc = this.validateFunc.bind(this);
    this.entreChatFunc = this.entreChatFunc.bind(this);
  }

  getUserPhone(e) {
    const userPhone = e.target.value
      .replace(/[{()}]/g, "")
      .replace("-", "")
      .replace(" ", "")
      .replace("  ", "");

    if (userPhone > 11 && userPhone !== "") {
      this.setState({
        userPhone: userPhone,
        enterChat: false,
        btnEnter: false
      });
    }
    this.validateFunc();
  }

  getUserName(e) {
    const userName = e.target.value;

    if (validator.isAlpha(userName) && userName !== "") {
      this.setState({
        userName: userName
      });
    }
    this.validateFunc();
  }

  validateFunc() {
    const { userName, userPhone } = this.state;

    if (userName !== "" && userPhone !== "") {
      this.setState({
        btnEnter: true
      });
    }
  }

  entreChatFunc() {
    const { btnEnter } = this.state;
    if (btnEnter) {
      this.setState({
        enterChat: true
      });
    }
  }

  render() {
    const { enterChat, btnEnter, userName, userPhone } = this.state;
    return (
      <root.div className="container-inner container-chat">
        <div className="chat-master">
          {enterChat ? (
            <div>
              <Listmsg username={userName} />
              <Textinput />
            </div>
          ) : (
            <div className="enter-chat">
              <input
                className="username-chat"
                type="chat"
                placeholder="Nome"
                onInput={e => this.getUserName(e)}
              />
              <InputMask
                className="phone-chat"
                mask="(99) 99999 - 9999"
                maskChar=" "
                placeholder="Telefone"
                onInput={e => this.getUserPhone(e)}
              />
              {btnEnter ? (
                <button onClick={this.entreChatFunc} className="btn-chat">
                  Entrar no chat
                </button>
              ) : (
                <button className="btn-chat-disabled">Entrar no chat</button>
              )}
            </div>
          )}
          <style type="text/css">{styles}</style>
        </div>
      </root.div>
    );
  }
}
